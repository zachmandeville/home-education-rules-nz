from openfisca_aotearoa.entities import Person, Family
from openfisca_core.model_api import *


class social_security__children_with_serious_disability(Variable):
    value_type = int
    entity = Family
    definition_period = MONTH
    label = "Input variable for a number of children in family with serious disability"
    reference = 'http://legislation.govt.nz/act/public/1964/0136/latest/DLM361659.html'

class social_security__eligible_for_child_disability_allowance(Variable):
    value_type = float
    entity = Family
    definition_period = MONTH
    label = "Eligibility for child disability allowance"
    reference = 'http://legislation.govt.nz/act/public/1964/0136/latest/DLM361659.html'

    def formula(families, period, parameters):
        return families('social_security__children_with_serious_disability', period) * 47.64